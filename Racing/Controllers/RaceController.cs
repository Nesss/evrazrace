﻿namespace Racing.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Racing.Models;

    /// <summary>
    /// Main controller.
    /// </summary>
    public class RaceController
    {
        private readonly string path = @"..\..\..\Files\config.txt";

        /// <summary>
        /// Main race method.
        /// </summary>
        /// <returns>result.</returns>
        public string StartRace()
        {
            string result = string.Empty;
            var routeLength = ReadRouteLength();
            var transportList = ReadTransport();
            var finishedTransport = new List<Transport>();
            foreach (var tr in transportList)
                result += tr.Start();

            while (transportList.Count != 0)
            {
                for (int i = 0; i < transportList.Count; i++)
                {
                    if (transportList[i].StopTime > 0)
                    {
                        transportList[i].StopTime--;
                        continue;
                    }

                    Random rnd = new Random();
                    double chance = rnd.Next(1000);
                    if (chance / 1000 <= transportList[i].BreakingChance)
                    {
                        result += transportList[i].Break();
                        transportList[i].StopTime = 5;
                        continue;
                    }

                    transportList[i].PassedDistance += transportList[i].Speed;
                    if (transportList[i].PassedDistance >= routeLength)
                    {
                        transportList[i].PassedDistance = routeLength;
                        finishedTransport.Add(transportList[i]);
                        result += transportList[i].Finish();
                        transportList.Remove(transportList[i]);
                        i--;
                        continue;
                    }

                    result += transportList[i].Report();
                }
            }

            for (int i = 0; i < finishedTransport.Count; i++)
                result += i + 1 + " место: " + finishedTransport[i].Name + Environment.NewLine;
            return result;
        }

        /// <summary>
        /// Get route length from file method.
        /// </summary>
        /// <returns>routeLength.</returns>
        public int ReadRouteLength()
        {
            int routeLength = 0;
            using StreamReader sr = new StreamReader(path, System.Text.Encoding.Default);
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                if (int.TryParse(line, out routeLength) == true)
                    break;
            }

            return routeLength;
        }

        /// <summary>
        /// Get transport list from file.
        /// </summary>
        /// <returns>transport list.</returns>
        public List<Transport> ReadTransport()
        {
            var transportList = new List<Transport>();
            using StreamReader sr = new StreamReader(path, System.Text.Encoding.Default);
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                string[] parameter = line.Split('|');
                switch (parameter[0])
                {
                    case "Truck":
                        var truck = new Truck(
                            parameter[1],
                            Convert.ToInt32(parameter[2]),
                            Convert.ToDouble(parameter[3]),
                            Convert.ToInt32(parameter[4]));
                        transportList.Add(truck);
                        break;
                    case "PassengerCar":
                        var car = new PassengerCar(
                            parameter[1],
                            Convert.ToInt32(parameter[2]),
                            Convert.ToDouble(parameter[3]),
                            Convert.ToInt32(parameter[4]));
                        transportList.Add(car);
                        break;
                    case "Bike":
                        var bike = new Bike(
                            parameter[1],
                            Convert.ToInt32(parameter[2]),
                            Convert.ToDouble(parameter[3]),
                            Convert.ToBoolean(parameter[4]));
                        transportList.Add(bike);
                        break;
                }
            }

            return transportList;
        }
    }
}
