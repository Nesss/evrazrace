﻿namespace Racing.Models
{
    using System;

    /// <summary>
    /// Bike class.
    /// </summary>
    public class Bike : Transport 
    {
        /// <summary>
        /// ctor.
        /// </summary>
        /// <param name="n">Name.</param>
        /// <param name="s">Speed.</param>
        /// <param name="b">BreakingChance.</param>
        /// <param name="c">WithSidecar.</param>
        public Bike(string n, int s, double b, bool c)
        {
            Name = n;
            Speed = s;
            BreakingChance = b;
            WithSidecar = c;
        }

        /// <summary>
        /// Is bike with sidecar or not.
        /// </summary>
        public bool WithSidecar { get; set; }

        /// <summary>
        /// Start race report method.
        /// </summary>
        public override string Start()
        {
            string report;
            if (WithSidecar == true)
            {
                report = string.Format(
                    "{0} начал движение со скоростью {1} м/с, шанс поломки: {2}. Снабжен коляской.{3}",
                    Name,
                    Speed,
                    BreakingChance,
                    Environment.NewLine);
            }
            else
            {
                report = string.Format(
                    "{0} начал движение со скоростью {1} м/с, шанс поломки: {2}. Коляска отсутствует.{3}",
                    Name,
                    Speed,
                    BreakingChance,
                    Environment.NewLine);
            }

            return report;
        }
    }
}
