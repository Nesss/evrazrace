﻿namespace Racing.Models
{
    using System;

    /// <summary>
    /// PassengerCar class.
    /// </summary>
    public class PassengerCar : Transport
    {
        /// <summary>
        /// ctor.
        /// </summary>
        /// <param name="n">Name.</param>
        /// <param name="s">Speed.</param>
        /// <param name="b">BreakingChance.</param>
        /// <param name="c">PassengerCount.</param>
        public PassengerCar(string n, int s, double b, int c)
        {
            Name = n;
            Speed = s;
            BreakingChance = b;
            PassengerCount = c;
        }

        /// <summary>
        /// Passenger count in car.
        /// </summary>
        public int PassengerCount { get; set; }

        /// <summary>
        /// Start passenger car method.
        /// </summary>
        public override string Start()
        {
            return string.Format(
                "{0} начал движение со скоростью {1} м/с, шанс поломки: {2}. Количество пассажиров: {3}.{4}",
                Name,
                Speed,
                BreakingChance,
                PassengerCount,
                Environment.NewLine);
        }
    }
}
