﻿namespace Racing.Models
{
    using System;

    /// <summary>
    /// Truck class.
    /// </summary>
    public class Truck : Transport
    {
        /// <summary>
        /// ctor.
        /// </summary>
        /// <param name="n">Name.</param>
        /// <param name="s">Speed.</param>
        /// <param name="b">BreakingChance.</param>
        /// <param name="c">CargoWeight.</param>
        public Truck(string n, int s, double b, int c)
        {
            Name = n;
            Speed = s;
            BreakingChance = b;
            CargoWeight = c;
        }

        /// <summary>
        /// CargoWeight.
        /// </summary>
        public int CargoWeight { get; set; }

        /// <summary>
        /// Start truck method.
        /// </summary>
        public override string Start()
        {
            return string.Format(
                "{0} начал движение со скоростью {1} м/с, шанс поломки: {2}. Вес груза: {3} кг.{4}",
                Name,
                Speed,
                BreakingChance,
                CargoWeight,
                Environment.NewLine);
        }
    }
}
