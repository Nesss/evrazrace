﻿namespace Racing.Models
{
    using System;

    /// <summary>
    /// Common transport class.
    /// </summary>
    public class Transport
    {
        /// <summary>
        /// Transport name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Transport speed.
        /// </summary>
        public int Speed { get; set; }

        /// <summary>
        /// Chance of wheel puncture.
        /// </summary>
        public double BreakingChance { get; set; }

        /// <summary>
        /// Passed distance.
        /// </summary>
        public double PassedDistance { get; set; } = 0;

        /// <summary>
        /// Time of stop cause of breaking.
        /// </summary>
        public int StopTime { get; set; } = 0;

        /// <summary>
        /// Common start method
        /// </summary>
        public virtual string Start()
        {
            return null;
        }

        /// <summary>
        /// Common distance report method.
        /// </summary>
        public string Report()
        {
            return Name + " проехал " + PassedDistance + " м." + Environment.NewLine;
        }

        /// <summary>
        /// Common break report method.
        /// </summary>
        public string Break()
        {
            return Name + " пробито колесо." + Environment.NewLine;
        }

        /// <summary>
        /// Common finish report method.
        /// </summary>
        public string Finish()
        {
            return Name + " финишировал." + Environment.NewLine;
        }
    }
}
