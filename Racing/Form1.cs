﻿namespace Racing
{
    using System;
    using System.Windows.Forms;
    using Racing.Controllers;

    /// <summary>
    /// Form1
    /// </summary>
    public partial class Form1 : Form
    {
        /// <summary>
        /// Form1
        /// </summary>
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Button1_Click
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void button1_Click(object sender, EventArgs e)
        {
            Button1.Enabled = false;
            var controller = new RaceController();
            textBox1.Text = controller.StartRace();
            Button1.Enabled = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
    }
}
